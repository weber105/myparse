//
//  NewNoteVC.swift
//  Parse
//
//  Created by Cody Weber on 9/8/15.
//  Copyright (c) 2015 Cody Weber. All rights reserved.
//

import UIKit

class NewNoteVC: UIViewController {

    @IBOutlet weak var noteTextView: UITextView!
    
    @IBAction func cancelNote(sender: UIBarButtonItem) {
        
        println("Note cancelled.")
        
        self.presentingViewController?.dismissViewControllerAnimated(true, completion: nil)
    }
    
    
    @IBAction func saveNote(sender: UIBarButtonItem) {
        
        saveNote()
        
        self.presentingViewController?.dismissViewControllerAnimated(true, completion: nil)
    }
    
    func saveNote() {
        println("You have to save the note.")
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
